<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

  <div class="register">
    <div class="col-lg-12">
    <?php
      if(Session::has('flash_message'))
      {
        $s=Session::get('flash_lavel');
        echo"<div class='$s'>";
        echo Session::get('flash_message') ;
        echo"</div>";
      }
    ?>
  </div>
  <a href='add'>Thêm sản phẩm</a>
  <h3>Danh sách sản phẩm</h3>   

  <table class="table table-bordered table-hover">
    <thead>
      <tr>
      	<th>Hình ảnh</th>
        <th>Tên</th>
        <th>Giá</th>
        <th>Thông Tin</th>
        <th>Chức năng</th>
        <th>Quốc Gia</th>
        <th>Chỉnh sửa</th>
      </tr>
    </thead>   
    <tbody>
    <?php
		foreach ($allproduct as $product ) 
		{
      
			echo"<tr>";
			$images = DB::table('hinhanh')->where('MSP', $product->MSP)->pluck('HinhAnh');
			echo"<td><a href='preview--{$product->MSP}'><img src=$images[0] width='70' height='70' alt=''/></a></td>";
      echo"<td>$product->Ten</td>";
      echo"<td>$product->Gia</td>";
      echo"<td>$product->ThongTin</td>";
      $nation= DB::table('quocgia')->where('MQG', $product->MQG)->pluck('TenQuocGia');
      echo"<td>$product->ChucNang</td>";
      echo"<td>$nation[0]</td>";
      echo"<td><a href='editproduct--$product->id'>Sửa</a>     <a href='delete/$product->MSP' >Xoá</a></td>";
      echo"</tr>";

		}
	?>	
    
    </tbody>
  </table>
  
</div>
</div>
</body>
