<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

  <div class="register">
    <div class="col-lg-12">
    <?php
      if(Session::has('flash_message'))
      {
        $s=Session::get('flash_lavel');
        echo"<div class='$s'>";
        echo Session::get('flash_message') ;
        echo"</div>";
      }
    ?>
  </div>
  <h3>Danh sách tài khoản</h3>   

  <table class="table table-bordered table-hover">
    <thead>
      <tr>
      	<th>id</th>
        <th>Email</th>
        <th>Tên chủ tài khoản</th>
        <th>Địa chỉ</th>
        <th>Số điện thoại</th>
        <th>Quyền hạn admin</th>
        <th>Chỉnh sửa</th>
      </tr>
    </thead>   
    <tbody>
		@foreach ($allaccount as $account ) 
      
			<tr>

      <td>{{$account->id}}</td>
      <td>{{$account->email}}</td>
      <td>{{$account->name}}</td>
      <td>{{$account->DiaChi}}</td>
      <?php
        if($account->qhan==0)
          $s="Tài khoản thường";
        else $s="Tài khoản admin";
      ?>
      <td>{{$account->SoDT}}</td>
      <td>{{$s}}</td>
      <td><a href='editaccount--{{$account->id}}'>Sửa</a>     <a href='editaccount/delete/{{$account->id}}' >Xoá</a></td>
      </tr>
      
		@endforeach	
    
    </tbody>
  </table>
</div>
</div>
</body>
