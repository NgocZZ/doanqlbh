
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home Shoppe</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


</head>
</html>
<body>
    	<div class="content_top">
    		<div class="back-links">
    	<?php
    		foreach ($products as $product ) {
    			$menus =DB::table('danhmuc')->where('MDM',$product->MDM)->pluck('TenDanhMuc');
	    			echo"<p><a href='index'>Trang chủ</a> >>>> <a href='product--{$product->MDM}''>$menus[0]</a></p>";
    		} 		
    	?>
    	    </div>
    		<div class="clear"></div>
    	</div>
    	<div class="section group">
				<div class="cont-desc span_1_of_2">
				  <div class="product-details">				
					<div class="grid images_3_of_2">
						<div id="container">
						   <div id="products_example">
							   <div id="products">

								<div class="slides_container">
								 <?php
							   		foreach ($products as $product ) {
    									$allimages = DB::select('select * from hinhanh where MSP=?',array($product->MSP));
    									$s=$product->MSP;
    									foreach($allimages as $images)
										echo"<a href='#' target='_blank'><img src='$images->HinhAnh' width='700' height='300' alt='' /></a>";	
									}
								?></div>
								<ul class="pagination">
								 <?php
							   		foreach ($products as $product ) {
    									$allimages = DB::select('select * from hinhanh where MSP=?',array($product->MSP));
    									foreach($allimages as $images)
										echo"<a href='#' target='_blank'><img src='$images->HinhAnh' width='70' height='70' alt='' /></a>";	
									}
								?></ul>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="desc span_3_of_2">
					 <?php
						foreach ($products as $product ) {
							echo"<h2>$product->Ten </h2>";
							echo"<p>$product->ChucNang </p>";					
						}
					?>
					<div class="price">
					 <?php
						foreach ($products as $product ) {

							echo"<p>Giá: <span>"; 	
							echo number_format($product->Gia);		
							echo" VND</span></p>";	
						}
					?>
						
					</div>
					<div class="available">
						<p>Lựa chọn :</p>
					<ul>
						<li>Màu:
							<select>
							<option>Bạc</option>
							<option>Đen</option>
							<option>Đen bóng</option>
							<option>Đỏ</option>
						</select></li>
						<li>Cỡ:<select>
							<option>Lớn</option>
							<option>Vừa</option>
							<option>nhở</option>
						</select></li>
						<li>Loại:<select>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</select></li>
					</ul>
					</div>
				<div class="share-desc">
					<div class="share">
						<p>Chia sẽ :</p>
						<ul>
					    	<li><a href="#"><img src="images/facebook.png" alt="" /></a></li>
					    	<li><a href="#"><img src="images/twitter.png" alt="" /></a></li>					    
			    		</ul>
					</div>
					<div class="button"><span><a href="#">Them vào giỏ</a></span></div>					
					<div class="clear"></div>
				</div>
				 <div class="wish-list">
				 	<ul>
				 		<li class="wish"><a href="#">Thêm vào danh sách muốn mua</a></li>
				 	    <li class="compare"><a href="#">Thêm vào danh sách chú ý</a></li>
				 	</ul>
				 </div>
			</div>
			<div class="clear"></div>
		  </div></div></div></div>
		<div class="product_desc">	
			<div id="horizontalTab">
				<ul class="resp-tabs-list">
					<li>Đánh giá sản phẩm</li>
					<li>Thông tin sản phẩm</li>
					<li>Khuyến mãi đi kèm</li>				
					<div class="clear"></div>
				</ul>
				<div class="resp-tabs-container">

				 <div class="product-tags">
				 	<?php
				      if(Session::has('flash_message'))
				      {
				        $s=Session::get('flash_lavel');
				        echo"<div class='$s'>";
				        echo Session::get('flash_message') ;
				        echo"</div>";
				      }
				    ?>	
					<form action="comment--{{$s}}" method="POST" class="  register-top-grid" enctype="multipart/form-data">
					 	<input type="hidden" name="_token" value="{!!csrf_token()!!}">
						<label for='usr'>Tên:</label>
						<?php
							if(Auth::check())
								echo"<input type='text' class='form-control' name='usr' value='$name' disabled>";
	            			else
	            				echo"<input type='text' class='form-control' name='usr'>";
	            		?>
	            		<label for="cm">Bình luận:</label>
	            		<input type="text" class="form-control" name="cm">
            			<button type="submit" class="btn btn-primary">Gửi bình luận </button>
					</form>
					<form class="form-horizontal">
				 	<div class="form-group">
					<?php
						foreach ($comment as $cm) {
							echo"<div class='content_top'>";
    						echo"<div class='back-links'>";
							echo"<label > Tên : $cm->Ten</label>";
							echo"<p></p>";
							echo"<label >Đánh giá : $cm->DGIA</label>";
							echo"<p></p>";
    	   				 	echo"</div>";
    						echo"<div class='clear'></div>";
    						echo"</div>";
						}
					?>
					</div>
					</form>
				
				</div>
					<div class="product-desc">
					<?php
						foreach ($products as $product ) 
						echo"<p>$product->ThongTin</p>";
					?>					
					</div>

				 <div class="product-tags">

						 <p>Vocher mua hàng 100.000VND</p>
				</div>
		</div>
	
	@include('composers.CategoryProductDL')
</body>