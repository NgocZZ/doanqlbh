<body>
<div class="wrap">
   <div class="footer ">
   	  	
	     <div class="section group">
				<div class="col_1_of_4 span_1_of_4">
						<h4>Thông tin</h4>
						<ul>
						<li><a href="about">Về chúng tôi</a></li>
						<li><a href="contact">Chăm sóc khách hàng</a></li>
						<li><a href="#">Kiểm tra đơn hàng</a></li>
						<li><a href="delivery">Đổi trả hàng</a></li>
						<li><a href="contact">Liên hệ với chúng tôi</a></li>
						</ul>
					</div>
				<div class="col_1_of_4 span_1_of_4">
					<h4>Hãy mua hàng từ hãng</h4>
						<ul>
						<li><a href="about">Về chúng tôi</a></li>
						<li><a href="contact">Chăm sóc khách hàng</a></li>
						<li><a href="#">Quy định</a></li>
						<li><a href="contact">Bản đồ</a></li>
						<li><a href="#">Tìm kiếm</a></li>
						</ul>
				</div>
				<div class="col_1_of_4 span_1_of_4">
					<h4>Tài khoản của tôi</h4>
						<ul>
							<li><a href="contact">Đăng nhập </a></li>
							<li><a href="index">Xem giỏ hàng</a></li>
							<li><a href="#">Đơn hàng của tôi</a></li>
							<li><a href="#">Kiểm tra đơn hàng</a></li>
							<li><a href="contact">Giúp đỡ</a></li>
						</ul>
				</div>
				<div class="col_1_of_4 span_1_of_4">
					<h4>Liên hệ</h4>
						<ul>
							<li><span>+91-123-456789</span></li>
							<li><span>+00-123-000000</span></li>
						</ul>
						<div class="social-icons">
							<h4>Theo dõi chúng tôi</h4>
					   		  <ul>
							      <li><a href="#" target="_blank"><img src="images/facebook.png" alt="" /></a></li>
							      <li><a href="#" target="_blank"><img src="images/twitter.png" alt="" /></a></li>
							      <li><a href="#" target="_blank"><img src="images/skype.png" alt="" /> </a></li>
							      <li><a href="#" target="_blank"> <img src="images/dribbble.png" alt="" /></a></li>
							      <li><a href="#" target="_blank"> <img src="images/linkedin.png" alt="" /></a></li>
							      <div class="clear"></div>
						     </ul>
   	 					</div>
				</div>
			</div>			
        </div>
    
    </div>
   <script type="text/javascript">
		$(document).ready(function() {			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
    <a href="#" id="toTop"><span id="toTopHover"> </span></a>
</body>
</html>