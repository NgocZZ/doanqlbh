<!DOCTYPE HTML>
<head>
<title>Free Home Shoppe </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
</head>

<body>
 	<div class="wrap">	
	<div class="header">
		<div class="headertop_desc">
			<div class="call">
				 <p><span>Cần giúp đỡ?</span> Gọi ngay <span class="number">1-22-3456789</span></span></p>
			</div>
			<div class="account_desc">
				 @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Đăng nhập</a></li>
                            <li><a href="{{ url('/register') }}">Đăng kí</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                   Xin chào, {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                	<li>
	                                    <a href="account" >
	                                   		Trang cá nhân
	                                	</a>
	                                </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Thoát
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                 
                                </ul>
                            </li>
                        @endif
			</div>
			<div class="clear"></div>
		</div>
		<div class="header_top">
			<div class="logo">
				<a href="index"><img src="images/logo.png" alt="" /></a>
			</div>
				  @if (!Auth::guest())
			  	   
			  <div class="cart">
			  
			  	   <p>Chào mừng bạn tới cửa hàng trưc tuyến! <span>Số lượng:</span><div id="dd" class="wrapper-dropdown-2"> 0 sản phẩm - 0.00 VND
			  	   	<ul class="dropdown">
							<li>Bạn không có sản phẩm nào trong giỏ hàng</li>
					</ul></div></p>
				
			  </div>
			  @endif
			  <script type="text/javascript">
			function DropDown(el) {
				this.dd = el;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.on('click', function(event){
						$(this).toggleClass('active');
						event.stopPropagation();
					});	
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd') );

				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-2').removeClass('active');
				});

			});

		</script>
	 <div class="clear"></div>
  </div>
</div>
  <div class="header_bottom">
	     	<div class="menu">
	     		<ul>
			    	<li><a href="index">Trang chủ</a></li>
			    	<li><a href="product">Sản phẩm</a></li>
			    	<li><a href="search?usr=Check+123">Tìm kiếm</a></li>
			    	<li><a href="about">Thông tin</a></li>
			    	<li><a href="delivery">Vận chuyển</a></li>
			    	<li><a href="contact">Liên hệ</a></li>
			    	<div class="clear"></div>
     			</ul>
	     	</div>
	     	<div class="search_box">
	     		<form action='search'>
	     			<input type="text" value="Tim kiem" name='usr' onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}"><input type="submit" value="">
	     		</form>
	     	</div>
	     	<div class="clear"></div>
	     </div>	     	
</div>

  </body>