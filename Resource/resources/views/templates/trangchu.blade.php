<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<head>
<title> Home Shoppe </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/slider.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/startstop-slider.js"></script>
</head>
<body>
  	<div class="wrap">	
	<div class="main">
	<div class="header_slide">
			<div class="header_bottom_left">				
				<div class="categories">
				  <ul>
				  	
				  	@include('composers.CategoryMenu')
				  </ul>
				</div>					
	  	     </div>
					 <div class="header_bottom_right">					 
					 	 <div class="slider">					     
							 <div id="slider">
			                    <div id="mover">
			                    	<div id="slide-1" class="slide">			                    
									 <div class="slider-img">
									     <a href="product"><img src="images/slide-1-image.png" alt="learn more" /></a>									    
									  </div>
						             	<div class="slider-text">
		                                 <h1>Xả hàng<br><span>HẠ GIÁ</span></h1>
		                                 <h2>TỚI 20%</h2>
									   <div class="features_list">
									   	<h4>Liên hệ với chúng tôi để nhận thêm ưu đãi</h4>							               
							            </div>
							             <a href="product" class="button">Mua ngay</a>
					                   </div>			               
									  <div class="clear"></div>				
				                  </div>	
						             	<div class="slide">
						             		<div class="slider-text">
		                                 <h1>Xả hàng<br><span>HẠ GIÁ</span></h1>
		                                 <h2>TỚI 40% </h2>
									   <div class="features_list">
									   	<h4>Liên hệ với chúng tôi để nhận những dịch vụ hậu mãi tốt nhất</h4>							               
							            </div>
							             <a href="#" class="button">Mua ngay</a>
					                   </div>		
						             	 <div class="slider-img">
									     <a href="#"><img src="images/slide-3-image.jpg" alt="learn more" /></a>
									  </div>						             					                 
									  <div class="clear"></div>				
				                  </div>
				                  <div class="slide">						             	
					                  <div class="slider-img">
									     <a href="#"><img src="images/slide-2-image.jpg" alt="learn more" /></a>
									  </div>
									  <div class="slider-text">
		                                 <h1>Xả hàng<br><span>HẠ GIÁ</span></h1>
		                                 <h2>TỚI 10% </h2>
									   <div class="features_list">
									   	<h4>Liên hệ với chúng tôi để mua hàng 1 cách tốt nhất</h4>							               
							            </div>
							             <a href="#" class="button">Mua ngay</a>
					                   </div>	
									  <div class="clear"></div>				
				                  </div>												
			                 </div>		
		                </div>
					 <div class="clear"></div>					       
		         </div>
		      </div>
		   <div class="clear"></div>
		</div>
   </div>
   </div>
</body>
  
</html>

