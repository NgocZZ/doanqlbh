<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<head>
<title>Free Home Shoppe Website Template | Delivery :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#posts").accordion({ 
			header: "div.tab", 
			alwaysOpen: false,
			autoheight: false
		});
	});
</script>
</head>
<body>

 <div class="wrap">	
	<div class="main">
    <div class="content">
    	<div class="section group">
				<div class="grid_1_of_3 images_1_of_3">
					  <img src="images/delivery-img1.jpg" alt="" />
					  <h3>Vận chuyển toàn quốc</h3>
					 </div>
				<div class="grid_1_of_3 images_1_of_3">
					  <img src="images/delivery-img2.jpg" alt="" />
					  <h3>Đội ngũ chuyên nghiệp </h3>
				</div>
				<div class="grid_1_of_3 images_1_of_3">
					  <img src="images/delivery-img3.jpg" alt="" />
					  <h3>Thanh toán tại nhà </h3>
				</div>
		
		</div>
		</div>
    </div>
 </div>
  </body>
</html>

