<!DOCTYPE HTML>
<head>
<title> Home Shoppe </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/slider.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/startstop-slider.js"></script>
</head>
<body>
		<div class="wrap">	
	<div class="main">
	    <div class="content">
	    	<div class="content_top">			
	     		<form action='search'>
	     			<label for="usr">Tìm kiếm:</label>
		  			<input type="text" class="form-control" name="usr">
		  			<label for="pwd">Giá tiền nhỏ hơn:</label>
					<input type="number" class="form-control" name="pri">
					<label for="pwd">Danh mục:</label>
					<select class="form-control" name="menu">
						<option value="0">Xin chọn danh mục</option>
						<?php
							foreach ($allcategory as $category ) {
								{
									echo"<option value='$category->MDM'>$category->TenDanhMuc</option>";
								}
							}
						?>
					
					<select>
					<label for="pwd">Quốc Gia:</label>
						<select class="form-control" name="nat">
						<option value="0">Xin chọn Quốc Gia</option>
						<?php
							foreach ($allnation as $nation ) {
								{
									echo"<option value='$nation->MQG'>$nation->TenQuocGia</option>";
								}
							}
						?>
					</select>	
			    	<input type="submit" value='Tìm kiếm'>
	     		</form>
	     		@include("composers.CategorySearch")
		    </div>
		</div>
	</div>
	</div>
</body>