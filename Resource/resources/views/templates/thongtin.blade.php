<!DOCTYPE HTML>
<head>
<title> Home Shoppe </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/slider.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/startstop-slider.js"></script>
</head>
<body>
 

 <div class="wrap">	
	<div class="main">
    <div class="content">
    	<div class="section group">
				<div class="col_1_of_3 span_1_of_3">
					<h3>Chúng tôi là</h3>
					<img src="images/about_img.jpg" alt="">
					
				</div>
				<div class="col_1_of_3 span_1_of_3">
					<h3>Lịch sử</h3>
				 <div class="history-desc">
					<div class="year"><p>2016 -</p></div>
					<p class="history">Thành lập</p>
				 <div class="clear"></div>
				</div>
				 
				</div>
			</div>			
    </div>
 </div>
</div>
 
</body>
</html>

