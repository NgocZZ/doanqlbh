<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use DB;
use Hash;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Htpp\Requests\LoginRequest;
use Illuminate\Contracts\Auth\Guard;
class PagesController extends Controller
{
	//trang chủ
	public function index(){
		$allcategory = DB::select('select * from danhmuc');
		return view("pages.index", ['allcategory' => $allcategory]);
	}

	// danh sách sản phẩm
	public function product(){
		$allcategory = DB::select('select * from danhmuc');
		return view("pages.product", ['allcategory' => $allcategory]);
	}

	//danh sách sản phẩm liên quan
	public function product2($id){
		$allcategory = DB::select('select * from danhmuc where MDM=?',array($id));
		return view("pages.product", ['allcategory' => $allcategory]);
	}

	//chi tiết snar phẩm
	public function preview($id){
		$name='';
		if(Auth()->check())
			$name=Auth()->user()->name;
		$product = DB::select('select * from sanpham where MSP=?',array($id));
		$comment= DB::select('select * from danhgiasp where MSP=?',array($id));
		foreach ($product as $pr ) 	
		$allcategory = DB::select('select * from danhmuc where MDM=?',array($pr->MDM));
		return view("pages.preview", ['products' => $product,'allcategory' => $allcategory,'comment'=>$comment,'name'=>$name]);
	}

	//tìm kiếm
	public function search(){
		$allcategory = DB::select('select * from danhmuc');
		$allnation = DB::select('select * from quocgia');
		return view("pages.search", ['allcategory' => $allcategory,'allnation' => $allnation]);	
	}

	//xoá sản phẩm và hình ảnh của sản phẩm
	public function getdelete($id){
		$allimages = DB::select("select * from hinhanh where MSP Like '%$id%'");
		foreach ($allimages as $images ) 
			Storage::delete('images->HinhAnh');	
		DB::delete("delete from sanpham where MSP Like '%$id%'");
		DB::delete("delete from hinhanh where MSP Like '%$id%'");
		return redirect()->route('Pages.edit')->with(['flash_lavel'=>'alert alert-success','flash_message'=>'Xoa Thanh cong']);
	}

	//xuất danh sách sản phẩm cho admin sửa chửa
	public function getedit(){
		$allproduct = DB::select('select * from sanpham ');
		return view("pages.edit", ['allproduct' => $allproduct]);
	}

	// xuất danh sach tài khoản cho admin chỉnh sửa
	public function getaccount(){
		$s=Auth::user()->id;
		$allaccount = DB::select('select * from users where id =? ',array($s));
		return view("pages.account", ['allaccount' => $allaccount]);
	}
	public function editaccount(){
		$s=Auth::user()->id;
		$allaccount = DB::select('select * from users where id <>? ',array($s));
		return view("pages.editaccount", ['allaccount' => $allaccount]);
	}

	// chỉnh sửa tìa khoản quyền admin
	public function editaccount1($id){
		$allaccount = DB::select('select * from users where id =? ',array($id));
		return view("pages.admineditacc", ['id'=>$id,'allaccount' => $allaccount]);
	}
	public function postaccount1(Request $request,$id){
		$fl='';
		$fm='';
		if($request->usr=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng không bỏ trống tên';
		}
		if($request->adr=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng không bỏ trống địa chỉ';
		}
		if($request->numb=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng không bỏ trống số điện thoại';
		}
		if($fl<>'')
				return redirect()->back()->with(['flash_lavel'=>$fl,'flash_message'=>$fm]);
		
		DB::table('users')->where('id', $id)->update(['name'=>$request->usr,'DiaChi'=>$request->adr,'SoDT'=>$request->numb,'qhan'=>$request->menu]);
		return redirect()->route('Pages.editaccount')->with(['flash_lavel'=>'alert alert-success','flash_message'=>'Luu Thanh cong']);
	}

	//chỉnh sửa tài khoản quyền thành viên
	public function getaccountdl(){
		$s=Auth::user()->id;
		$allaccount = DB::select('select * from users where id =? ',array($s));
		return view("pages.editaccountdl", ['allaccount' => $allaccount]);
	}
	public function postaccountdl(Request $request){
		$s=Auth::user()->id;
		$fl='';
		$fm='';
		if($request->usr=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng không bỏ trống tên';
		}
		if($request->adr=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng không bỏ trống địa chỉ';
		}
		if($request->numb=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng không bỏ trống số điện thoại';
		}
		if($fl<>'')
				return redirect()->back()->with(['flash_lavel'=>$fl,'flash_message'=>$fm]);
		
		DB::table('users')->where('id', $s)->update(['name'=>$request->usr,'DiaChi'=>$request->adr,'SoDT'=>$request->numb]);
		return redirect()->route('Pages.account')->with(['flash_lavel'=>'alert alert-success','flash_message'=>'Luu Thanh cong']);
	}

	//xoá user khỏi hệ thống
	public function getdelete1($id)
	{
		DB::delete("delete from users where id Like '%$id%'");
		return redirect()->route('Pages.editaccount')->with(['flash_lavel'=>'alert alert-success','flash_message'=>'Xoa Thanh cong']);
	}

	//thay đổi passowrd
	public function getchangepassword(){

	return view("pages.changepassword");	
	}
	public function postchangepassword(Request $request){
		 if (Hash::check($request->oldpassword, Auth::user()->password)==1)
		 {
		 	if($request->password!=$request->password_confirmation)
		 		return redirect()->route('Pages.changepassword')->with(['flash_lavel'=>'alert alert-danger','flash_message'=>'Yêu cầu nhập pass giống nhau']);
		 	$password = Hash::make($request->password);
		 	$s=Auth::user()->id;	
		 	DB::table('users')->where('id', $s)->update(['password'=>$password]);
		 	return redirect()->route('Pages.account')->with(['flash_lavel'=>'alert alert-success','flash_message'=>'Đổi pass thanh cong']);	
		 }
		 else
		 return redirect()->route('Pages.changepassword')->with(['flash_lavel'=>'alert alert-danger','flash_message'=>'Yêu cầu nhập pass cũ chính xác ']);
	}

	//thêm sản phẩm
	public function getadd(){
		$allcategory = DB::select('select * from danhmuc');
		$allnation = DB::select('select * from quocgia');
		return view("pages.add", ['allcategory' => $allcategory,'allnation' => $allnation]);	
		
	}
	public function postadd(Request $request){
		$allproduct = DB::select('select * from sanpham ');
		$s=0;
		$check=0;
		$fl='';
		$fm='';
		if($request->file('img')== null)
		{
			$fl='alert alert-danger';
			$fm='Vui lòng chọn file ảnh';
		}
		if($request->nat=='0')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng chọn quốc gia';
		}
		if($request->menu=='0')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập danh mục';
		}
		if($request->opt=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập chức nâng';
		}
		if($request->inf=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập thông tin';
		}
		if($request->pri=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập giá';
		}
		if($request->usr=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập tên';
		}
		if($fl<>'')
				return redirect()->route('Pages.getadd')->with(['flash_lavel'=>$fl,'flash_message'=>$fm]);
		
		foreach ($allproduct as $product ) 
		{
			$s=$s+1;
			$check=0;
			foreach ($allproduct as $product1 ) 
			if($s==$product1->MSP) 
			{
				$check=1;
				break;
			}
			if($check==0) break;
		}
		if($check==1) $s=$s+1;
		$s=$s . $request->menu;
		$file_name=$request->file('img')->getClientOriginalName(); 
		$product= new sanpham();
		$product->MSP=$s;
		$product->MDM=$request->menu;
		$product->Ten=$request->usr;
		$product->Gia=$request->pri;
		$product->MQG=$request->nat;
		$product->ThongTin=$request->inf;
		$product->ChucNang=$request->opt;
		$request->file('img')->move('images',$file_name);
		$product->save();
		$images= new hinhanh();
		$images->MSP=$s;
		$images->HinhAnh="images/" . $file_name;
		$images->save();
	return redirect()->route('Pages.getadd')->with(['flash_lavel'=>'alert alert-success','flash_message'=>'Them thanh cong']);
	}

	// chỉnh sửa sản phẩm	
	public function geteditproduct($id){
		$allproduct=DB::select('select * from sanpham where id=?',array($id));
		$allcategory = DB::select('select * from danhmuc');
		$allnation = DB::select('select * from quocgia');
		return view("pages.editproduct", ['allproduct' => $allproduct,'allcategory' => $allcategory,'allnation' => $allnation]);	
	}
	public function posteditproduct(Request $request,$id)
	{
		$fl='';
		$fm='';
		if($request->opt=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập chức năng mới';
		}
		if($request->inf=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập thông tin mới';
		}
		if($request->pri=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập giá mới';
		}
		if($request->usr=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập tên mới' ;
		}
		if($fl<>'')
				return redirect()->route('Pages.geteditproduct',[$id])->with(['flash_lavel'=>$fl,'flash_message'=>$fm]);

		$product=sanpham::find($id);
		$product->MDM=$request->menu;
		$product->Ten=$request->usr;
		$product->Gia=$request->pri;
		$product->MQG=$request->nat;
		$product->ThongTin=$request->inf;
		$product->ChucNang=$request->opt;
		$product->save();
		return redirect()->route('Pages.edit',[$id])->with(['flash_lavel'=>'alert alert-success','flash_message'=>'Sua thanh cong']);
	}
	public function postedit(){
		$allproduct = DB::select('select * from sanpham ');
		return view("pages.edit", ['allproduct' => $allproduct]);
	}


    //tạo comment
   public function postcomment(request $request,$msp)
   {
   		$fl='';
		$fm='';
		$name='';
		if (Auth()->check())
			$name=Auth()->user()->name;
		else
			$name=$request->usr;
   		if($name=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập tên ' ;
		}
		if($request->cm=='')
		{
			$fl='alert alert-danger';
			$fm='Vui lòng nhập bình luận' ;
		}
		if($fl<>'')
		return redirect()->back()->with(['flash_lavel'=>$fl,'flash_message'=>$fm]);
   		$comment= new binhluan();
   		$comment->MSP=$msp;
   		$comment->Ten=$name;
   		$comment->DGIA=$request->cm;
   		$comment->save();
   		return redirect()->back();
   }
}
