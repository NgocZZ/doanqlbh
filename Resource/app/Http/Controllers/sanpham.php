<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;

class sanpham extends Model
{
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='sanpham';
    protected $fillable = ['id','MSP','MDM', 'Ten', 'Gia','ThongTin','ChucNang','MQG'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
