-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 04, 2017 lúc 01:56 CH
-- Phiên bản máy phục vụ: 5.7.14
-- Phiên bản PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `qlbh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danhgiasp`
--

CREATE TABLE `danhgiasp` (
  `MDG` int(11) NOT NULL,
  `MSP` varchar(250) DEFAULT NULL,
  `DGIA` varchar(250) DEFAULT NULL,
  `Ten` varchar(250) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danhmuc`
--

CREATE TABLE `danhmuc` (
  `MDM` varchar(30) NOT NULL,
  `TenDanhMuc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `danhmuc`
--

INSERT INTO `danhmuc` (`MDM`, `TenDanhMuc`) VALUES
('DDD', 'Đồ da dụng'),
('DDT', 'Đồ điện tử'),
('LK', 'Linh kiện'),
('LT', 'Điện thoại');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `donhang`
--

CREATE TABLE `donhang` (
  `MDH` varchar(30) NOT NULL,
  `MTK` varchar(30) NOT NULL,
  `NgayMua` date NOT NULL,
  `TongTien` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hinhanh`
--

CREATE TABLE `hinhanh` (
  `MSP` varchar(30) NOT NULL,
  `HinhAnh` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `hinhanh`
--

INSERT INTO `hinhanh` (`MSP`, `HinhAnh`, `created_at`, `updated_at`) VALUES
('001LK', 'images/001LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('001LK', 'images/001LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('001LK', 'images/001LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('001LK', 'images/001LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('001LK', 'images/001LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('002LK', 'images/002LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('002LK', 'images/002LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('002LK', 'images/002LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('002LK', 'images/002LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('002LK', 'images/002LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('003LK', 'images/003LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('003LK', 'images/003LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('003LK', 'images/003LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('003LK', 'images/003LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('003LK', 'images/003LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('004LK', 'images/004LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('004LK', 'images/004LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('004LK', 'images/004LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('004LK', 'images/004LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('004LK', 'images/004LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('005LK', 'images/005LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('005LK', 'images/005LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('005LK', 'images/005LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('005LK', 'images/005LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('005LK', 'images/005LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('006LT', 'images/006LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('006LT', 'images/006LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('006LT', 'images/006LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('006LT', 'images/006LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('006LT', 'images/006LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('007LT', 'images/007LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('007LT', 'images/007LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('007LT', 'images/007LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('007LT', 'images/007LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('007LT', 'images/007LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('008LT', 'images/008LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('008LT', 'images/008LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('008LT', 'images/008LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('008LT', 'images/008LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('008LT', 'images/008LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('009LT', 'images/009LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('009LT', 'images/009LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('009LT', 'images/009LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('009LT', 'images/009LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('009LT', 'images/009LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('010LT', 'images/010LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('010LT', 'images/010LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('010LT', 'images/010LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('010LT', 'images/010LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('010LT', 'images/010LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('011DDT', 'images/011DDT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('011DDT', 'images/011DDT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('011DDT', 'images/011DDT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('011DDT', 'images/011DDT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('011DDT', 'images/011DDT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('012DDT', 'images/012DDT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('012DDT', 'images/012DDT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('012DDT', 'images/012DDT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('012DDT', 'images/012DDT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('012DDT', 'images/012DDT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('013DDT', 'images/013DDT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('013DDT', 'images/013DDT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('013DDT', 'images/013DDT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('013DDT', 'images/013DDT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('013DDT', 'images/013DDT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('014DDD', 'images/014DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('014DDD', 'images/014DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('014DDD', 'images/014DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('014DDD', 'images/014DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('014DDD', 'images/014DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('015DDD', 'images/015DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('015DDD', 'images/015DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('015DDD', 'images/015DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('015DDD', 'images/015DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('015DDD', 'images/015DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('016DDD', 'images/016DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('016DDD', 'images/016DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('016DDD', 'images/016DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('016DDD', 'images/016DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('016DDD', 'images/016DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('017DDD', 'images/017DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('017DDD', 'images/017DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('017DDD', 'images/017DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('017DDD', 'images/017DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('017DDD', 'images/017DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('018DDD', 'images/018DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('018DDD', 'images/018DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('018DDD', 'images/018DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('018DDD', 'images/018DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('018DDD', 'images/018DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('019DDD', 'images/019DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('019DDD', 'images/019DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('019DDD', 'images/019DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('019DDD', 'images/019DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('019DDD', 'images/019DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('020DDD', 'images/020DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('020DDD', 'images/020DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('020DDD', 'images/020DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('020DDD', 'images/020DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('020DDD', 'images/020DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('021DDD', 'images/021DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('021DDD', 'images/021DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('021DDD', 'images/021DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('021DDD', 'images/021DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('021DDD', 'images/021DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('022LT', 'images/022LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('022LT', 'images/022LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('022LT', 'images/022LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('022LT', 'images/022LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('022LT', 'images/022LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('023LT', 'images/023LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('023LT', 'images/023LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('023LT', 'images/023LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('023LT', 'images/023LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('023LT', 'images/023LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('024LT', 'images/024LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('024LT', 'images/024LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('024LT', 'images/024LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('024LT', 'images/024LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('024LT', 'images/024LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('025LT', 'images/025LT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('025LT', 'images/025LT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('025LT', 'images/025LT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('025LT', 'images/025LT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('025LT', 'images/025LT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('026DDT', 'images/026DDT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('026DDT', 'images/026DDT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('026DDT', 'images/026DDT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('026DDT', 'images/026DDT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('026DDT', 'images/026DDT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('027DDT', 'images/027DDT001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('027DDT', 'images/027DDT002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('027DDT', 'images/027DDT003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('027DDT', 'images/027DDT004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('027DDT', 'images/027DDT005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('028DDD', 'images/028DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('028DDD', 'images/028DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('028DDD', 'images/028DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('028DDD', 'images/028DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('028DDD', 'images/028DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('029DDD', 'images/029DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('029DDD', 'images/029DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('029DDD', 'images/029DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('029DDD', 'images/029DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('029DDD', 'images/029DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('030DDD', 'images/030DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('030DDD', 'images/030DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('030DDD', 'images/030DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('030DDD', 'images/030DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('030DDD', 'images/030DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('031DDD', 'images/031DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('031DDD', 'images/031DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('031DDD', 'images/031DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('031DDD', 'images/031DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('031DDD', 'images/031DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('032DDD', 'images/032DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('032DDD', 'images/032DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('032DDD', 'images/032DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('032DDD', 'images/032DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('032DDD', 'images/032DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('033DDD', 'images/033DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('033DDD', 'images/033DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('033DDD', 'images/033DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('033DDD', 'images/033DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('033DDD', 'images/033DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('034DDD', 'images/034DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('034DDD', 'images/034DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('034DDD', 'images/034DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('034DDD', 'images/034DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('034DDD', 'images/034DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('035DDD', 'images/035DDD001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('035DDD', 'images/035DDD002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('035DDD', 'images/035DDD003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('035DDD', 'images/035DDD004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('035DDD', 'images/035DDD005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('036LK', 'images/036LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('036LK', 'images/036LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('036LK', 'images/036LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('036LK', 'images/036LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('036LK', 'images/036LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('037LK', 'images/037LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('037LK', 'images/037LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('037LK', 'images/037LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('037LK', 'images/037LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('037LK', 'images/037LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('038LK', 'images/038LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('038LK', 'images/038LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('038LK', 'images/038LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('038LK', 'images/038LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('038LK', 'images/038LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('039LK', 'images/039LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('039LK', 'images/039LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('039LK', 'images/039LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('039LK', 'images/039LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('039LK', 'images/039LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('040LK', 'images/040LK001.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('040LK', 'images/040LK002.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('040LK', 'images/040LK003.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('040LK', 'images/040LK004.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19'),
('040LK', 'images/040LK005.jpg', '2016-12-01 00:21:19', '2016-12-01 00:21:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(250) CHARACTER SET utf8 NOT NULL,
  `token` varchar(250) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('123@123.com', '5d94067e2ad3f34d06f3c8bcb16c83c0d2e0fd0987c79a81d881130f3d13e7db', '2017-01-04 04:32:03'),
('hackerlovely0102@gmail.com', 'ae3363c33873b754b999dba309949c9b3316941739d650e167779f817ac7ccfd', '2017-01-04 04:29:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `quocgia`
--

CREATE TABLE `quocgia` (
  `MQG` varchar(20) NOT NULL,
  `TenQuocGia` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `quocgia`
--

INSERT INTO `quocgia` (`MQG`, `TenQuocGia`) VALUES
('HQ', 'Hàn Quốc'),
('My', 'Mỹ'),
('NB', 'Nhật Bản'),
('TQ', 'Trung Quốc'),
('VN', 'Việt Nam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `MSP` varchar(30) NOT NULL,
  `MDM` varchar(30) NOT NULL,
  `Ten` varchar(255) DEFAULT NULL,
  `Gia` int(11) DEFAULT NULL,
  `ThongTin` varchar(255) DEFAULT NULL,
  `ChucNang` varchar(255) DEFAULT NULL,
  `MQG` varchar(20) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`MSP`, `MDM`, `Ten`, `Gia`, `ThongTin`, `ChucNang`, `MQG`, `updated_at`, `created_at`, `id`) VALUES
('001LK', 'LK', 'Chuot co day Zadez M218', 100001, 'Nho gon, thoai mai, day rut 80cm', 'Tuong thich voi nhieu he dieu hanh', 'TQ', '2017-01-03 21:34:24', '2016-12-01 00:21:19', 1),
('002LK', 'LK', 'Tai nghe bluetooth Roman R552S', 250000, 'Nho gon, thoai mai, thoi gian thoai len den 6 gio', 'Nghe nhac voi am luong tu 6 toi 10', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 2),
('003LK', 'LK', 'Pin sac du phong 5000mah eValu Roller 2', 200001, 'Su dung loi pin li-ion, den led bao pin', 'Nho gon, de mang theo', 'TQ', '2017-01-04 04:48:31', '2016-12-01 00:21:19', 3),
('004LK', 'LK', 'Op lung Xperia M5 Dual', 500000, 'Nhua deo, mau cam', 'ben bi, gia cong sac xao', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 4),
('005LK', 'LK', 'Pin sac du phong 7500 mAh ECO Y39A', 250000, '2 cong sac 1A va 2.1A, loi pin Li-ion', 'Co den LED bao trang thai pin tien dung', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 5),
('006LT', 'LT', 'May tinh bang Samsung Galaxy Tab A 9.7', 5841000, 'Camera truoc 5MP, camerasau 2MP, quay phim HD 720p', 'Nho gon, tien loi, lam viec voi but S-pen', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 6),
('007LT', 'LT', 'May tinh bang Huawei MediaPad T2 7 Pro', 4590000, 'Camera truoc 13MP, camerasau 5MP, quay phim full HD 1080p', 'Nho gon, tien loi, nhan dien khuon mat', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 7),
('008LT', 'LT', 'May tinh bang Acer Iconia Talk S A1-734', 3190000, 'Camera truoc 13MP, camerasau 2MP, quay phim full HD 1080p', 'Nho gon, tien loi, nhan dien khuon mat', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 8),
('009LT', 'LT', 'May tinh bang Lenovo Tab 3', 2390000, 'Camera truoc 5MP, camerasau 2MP', 'Nho gon, tien loi, tu dong lay net', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 9),
('010LT', 'LT', 'May tinh bang iPad Mini 4 Wifi 32GB', 10490000, 'Camera truoc 8MP, camerasau 1.2MP, quay phim full HD 1080p', 'Nho gon, tien loi, HDR, nhan dien nu cuoi', 'My', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 10),
('011DDT', 'DDT', 'Tu Lanh Panasonic NR-BA228PSVN', 6290000, 'Cam bien Econavi dieu chinh nhiet do linh hoat', 'Khu mui, khang khuan', 'NB', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 11),
('012DDT', 'DDT', 'May Giat Toshiba 8.2kg AW-MF920LV WK', 5690000, 'Long giat Magic Drum', 'Cong nghe giat bot khi tay sach moi vet ban', 'NB', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 12),
('013DDT', 'DDT', 'Dieu hoa Daikin 1 HP FTKC25QVMV', 11290000, 'Cong nghe Inverter', 'Cam bien thong minh tiet kiem dien', 'NB', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 13),
('014DDD', 'DDD', 'Noi com dien Delites 1.8l NCG1801', 300000, 'Long noi bang hop kim nhom chong dinh', 'Nau com, ham nong', 'HQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 14),
('015DDD', 'DDD', 'Bep hong ngoai Midea MIR-B2015DD', 500000, 'Su dung duoc tat ca loai noi', 'De su dung, hen gio', 'HQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 15),
('016DDD', 'DDD', 'Lo vi song Midea 20 lit MMO-20KE1', 1000000, 'Dung tich 20 lit', 'De su dung, giao dien tieng viet, hen gio', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 16),
('017DDD', 'DDD', 'Bep ga Kiwa KW-770SG', 500000, 'Kinh cuong luc, dau dot bang dong thau', 'Chiu luc, nhiet, tiet kiem gas', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 17),
('018DDD', 'DDD', 'Binh thuy dien Panasonic NC-EG3000CSY 3 lít', 1500000, 'Dung tich 3 lit, cong suat 700W ', '4 che do giu nhiet, hen gio dun soi lai', 'NB', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 18),
('019DDD', 'DDD', 'Lo nuong Sanaky VH359S 35 lit', 1350000, 'Quat doi luu, xien quay, kinh cuong luc 2 lop ', 'Thuc pham chin deu, khong bi chay, giu nhiet tot', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 19),
('020DDD', 'DDD', 'Quat lung Midea FS40-15VD', 380000, 'Trang bi 3 canh quat sai 40cm ', '3 toc do gio tuy chinh', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 20),
('021DDD', 'DDD', 'Ban ui kho Delites BUK02', 100000, 'Mat ban ui phu lop chong dinh, cong suat 1000W ', 'Ui thang quan ao nhanh, it ton dien', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 21),
('022LT', 'LT', 'Dien thoai OPPO F1s', 5990000, 'Man hinh 5.5 Inch, Camera truoc 16MP ', 'Cam ung da diem, quay phim full HD', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 22),
('023LT', 'LT', 'May tinh bang iPad Mini 2 Retina 16G/Wifi', 5990000, 'Man hinh 7.9 Inch, Man hinh Retina cong nghe IPS ', 'Cam ung da diem, quay phim full HD', 'My', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 23),
('024LT', 'LT', 'Lap top Asus A540LA i3 5005U/4GB/500GB/Win10', 9990000, 'Dai 380 mm - ngang 252 mm - day 25 mm. Trong luong 1.9kg ', 'Gia xuoc, sang trong', 'VN', '2017-01-01 07:07:49', '2017-01-01 07:07:49', 24),
('025LT', 'LT', 'Lap top Dell Inspiron 3558 i5 5200U/4GB/500GB/2GB 820M/Win10', 13690000, 'Dai 381.4 mm - ngang 267.6 mm - day 25.6 mm. Trong luong 2.4kg ', 'Vuong van, thoai mai, de chiu', 'My', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 25),
('026DDT', 'DDT', 'Loa vi tinh Fenda F380X', 13500000, 'Nguon 220V - 240V, Cong suat 54W, thiet ke dep mat, ti mi ', 'Chat luong am thanh song dong, remote chinh bai nhac', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 26),
('027DDT', 'DDT', 'Smart Tivi LG 43 inch 43LH600T', 9990000, 'Thiet ke kim loai, sac sao, an tuong ', 'Chip xu ly Trip XD Engine, giao dien WebOS dep mat', 'NB', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 27),
('028DDD', 'DDD', 'May nuoc nong Ferroli Divo SSN 4.5S (BM01) 4.5 kW', 1790000, 'Thanh dien tro bang dong, voi sen 6 kieu phun', 'Giong dien giat', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 28),
('029DDD', 'DDD', 'Cay nuoc nong lanh Kangaroo KG34C', 1790000, 'Kieu dang nguyen khoi gon gang', '2 voi rot nuoc rieng biet, Tich hop khoang khu trung tien loi', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 29),
('030DDD', 'DDD', 'May say quan ao Sunhouse SHD 2616', 1190000, 'Thiet ke gon nhe chac chan', 'Say kho trong vong 1 tieng, trong luong say 10kg, khong tao tieng on', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 30),
('031DDD', 'DDD', 'Chao chong dinh Sunhouse CT14 14 cm', 30000, 'Thiet ke nho gon, don gian', 'Hop kim nhom chong dinh, dan nhiet tot', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 31),
('032DDD', 'DDD', 'May danh trung Philips HR1459', 700000, 'Thiet ke sang trong vo nhua ABS cao cap, han che bam ban, de ve sinh', 'Cong suat manh me 300W, 5 toc do quay', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 32),
('033DDD', 'DDD', 'May ep trai cay Pensonic PJ-300', 700000, 'Thiet ke nho gon, mau sac trang nha', 'Ong tiep nguyen lieu duong kinh lon, cong suat 400W', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 33),
('034DDD', 'DDD', 'Noi ap suat Goldsun PC-GJY50W 5.0 lit', 400000, 'Du nau an cho 2-3 nguoi', 'Hop kim nhom sang bong, toa nhiet deu va nhanh', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 34),
('035DDD', 'DDD', 'May say toc Goldsun HD-GXD850', 100000, 'Nho gon, it bam bui, de ve sinh', 'Vo nhua bong, khong bam ban, 2 toc do gio', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 35),
('036LK', 'LK', 'Chuot khong day Genius NX 7010', 200000, 'Nho gon, don gian, chac chan', 'Su dung tren nhieu mat phang, tuong thich nhieu he dieu hanh', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 36),
('037LK', 'LK', 'Chuot co day Newmen N500 Trang - KG', 220000, 'Tre trung, nang dong, the thao', 'Su dung tren nhieu mat phang, Nut dieu chinh 4 muc DPI', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 37),
('038LK', 'LK', 'Tai nghe EP Awei Q7Ni', 150000, 'Chat lieu nhua deo cao cap, don gian, dep mat', 'Lop dem em khong gay kho chiu khi deo lau, thiet ke chong roi', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 38),
('039LK', 'LK', 'USB Sandisk SDCZ50 8GB 2.0 Xanh duong', 120000, 'Thoai mai luu tru', 'Hang chat luong', 'TQ', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 39),
('040LK', 'LK', 'O cam 5 lo 3 chau day 5m Dien Quang SM750SL', 190000, 'Thiet ke nho gon, tien loi', 'Lo cam co nap che, vo ngoai nhua ABS, chong ri, cong tac nguon tu dong', 'VN', '2016-12-01 00:21:19', '2016-12-01 00:21:19', 40);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(250) CHARACTER SET utf32 NOT NULL,
  `password` varchar(250) CHARACTER SET utf32 NOT NULL,
  `qhan` int(11) DEFAULT '0',
  `name` varchar(250) CHARACTER SET utf32 NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL,
  `remember_token` varchar(250) NOT NULL DEFAULT 'a',
  `DiaChi` varchar(250) CHARACTER SET utf32 NOT NULL DEFAULT 'Còn trống',
  `SoDT` varchar(13) CHARACTER SET utf32 NOT NULL DEFAULT 'Còn Trống'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `qhan`, `name`, `updated_at`, `created_at`, `remember_token`, `DiaChi`, `SoDT`) VALUES
(9, '1234@1234.com', '$2y$10$MVC/sN2y76EG8ipR44Rp2e.BiYgdJ4.sIjG4r23X6UhVXKlsJrW..', 0, '123', '2017-01-01 03:39:05', '2017-01-01 02:46:02', 'gSwppHbGiE56c00MZv8B8gWNWPhWDAOd87hPw8YBhBzmYcp1vbO9p4eWK4U0', 'a', 'b'),
(8, '123@123.com', '$2y$10$h7078u.2BKhdl5JToEl3bOoQPbBzDgOJH5XK2lyAEFKH94wesfz2O', 1, '123', '2017-01-04 06:46:02', '2017-01-01 09:34:35', '0umaAKefd825YUHX0mYUoZjE9u2KKJYPsAFVY3r37hsof1o15rfBGI1tLVte', 'a', 'b'),
(10, 'hackerlovely0102@gmail.com', '$2y$10$6jjIJW9oUkVlfv.Jkh51ve9wLXVMy1V15z5nHitpvwNofdwU3eC1G', 0, '123', '2017-01-04 04:17:58', '2017-01-04 04:17:51', 'YVKWIXPeP2oXQ8J1xznAj6Scj0U52HawdsLqtODU6Y7BwGWLcrZJ7sus1HGj', 'Còn trống', 'Còn Trống');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `danhgiasp`
--
ALTER TABLE `danhgiasp`
  ADD PRIMARY KEY (`MDG`);

--
-- Chỉ mục cho bảng `danhmuc`
--
ALTER TABLE `danhmuc`
  ADD PRIMARY KEY (`MDM`);

--
-- Chỉ mục cho bảng `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`MDH`),
  ADD KEY `MTK` (`MTK`);

--
-- Chỉ mục cho bảng `hinhanh`
--
ALTER TABLE `hinhanh`
  ADD PRIMARY KEY (`MSP`,`HinhAnh`),
  ADD KEY `MSP` (`MSP`),
  ADD KEY `MSP_2` (`MSP`);

--
-- Chỉ mục cho bảng `quocgia`
--
ALTER TABLE `quocgia`
  ADD PRIMARY KEY (`MQG`),
  ADD UNIQUE KEY `MQG` (`MQG`),
  ADD KEY `MQG_2` (`MQG`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sanpham_ibfk_1` (`MDM`),
  ADD KEY `MQG` (`MQG`),
  ADD KEY `MSP` (`MSP`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `danhgiasp`
--
ALTER TABLE `danhgiasp`
  MODIFY `MDG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `sanpham_ibfk_1` FOREIGN KEY (`MDM`) REFERENCES `danhmuc` (`MDM`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sanpham_ibfk_2` FOREIGN KEY (`MQG`) REFERENCES `quocgia` (`MQG`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
